<?php
//Подключение

include 'config.php';
include 'classes/Page.php';
include 'classes/Database.php';
include 'classes/Auth.php';





$page = new \classes\Page();
$auth = new \classes\Auth();
//var_dump($_SERVER['DOCUMENT_ROOT']);
//страница............

$a = $page->count_goods_int();
$count_goods = intval($a['0']);
$count_pages = ceil($count_goods / $perpage);
if (!$count_goods) {
    $count_goods = 1;
}
if (!isset($_GET['pages']) || (($_GET['pages'] <= 0))) {
    $_GET['pages'] = 1;
}
$pages = intval($_GET['pages']);
if ($pages > $count_pages) {
    $pages = $count_pages;
}
$start_pos = ($pages - 1) * $perpage;

//страница............


if (isset($_COOKIE['id'])) {
    if (isset($_COOKIE['id']) && isset($_COOKIE['hash'])) {
        $id_cook = filter_input(INPUT_COOKIE, 'id', FILTER_SANITIZE_NUMBER_INT);
        $hash_cook = filter_input(INPUT_COOKIE, 'hash', FILTER_SANITIZE_URL);
        // var_dump($id_cook);


        $auth->check_the_connection_of_loyalty();

    } else {
        print "Включите куки";
    }
}


if (isset($_POST['login']) && isset($_POST['password']) && (isset($_POST['submit']))) {
    $login = filter_input(INPUT_POST, 'login', FILTER_SANITIZE_URL);
    $password = filter_input(INPUT_POST, 'password', FILTER_SANITIZE_URL);
    // $not_attach_ip=($_POST['not_attach_ip']);
    $auth->authoriz($login, $password);

}


if (isset($_POST['login']) && isset($_POST['password']) && (!isset($_POST['submit']))) {
    $login = filter_input(INPUT_POST, 'login', FILTER_SANITIZE_URL);
    $password = filter_input(INPUT_POST, 'password', FILTER_SANITIZE_URL);
    $auth->inspection_new_user($login, $password);


}

if (isset($_POST['title_upd']) && isset($_POST['descr_upd']) && isset($_POST['text_upd']) && isset($_POST['id_upd'])) {
    $id_upd = filter_input(INPUT_POST, 'id_upd', FILTER_SANITIZE_FULL_SPECIAL_CHARS);
    $title_upd = filter_input(INPUT_POST, 'title_upd', FILTER_SANITIZE_FULL_SPECIAL_CHARS);
    $descr_upd = filter_input(INPUT_POST, 'descr_upd', FILTER_SANITIZE_FULL_SPECIAL_CHARS);
    $text_upd = filter_input(INPUT_POST, 'text_upd', FILTER_SANITIZE_FULL_SPECIAL_CHARS);
    $page->edit_db($title_upd, $descr_upd, $text_upd, $id_upd);


    header('Location:index.php?id=admin');

}

if (isset($_POST['id_t_del'])) {
    $id_t_del = (int)$_POST['id_t_del'];
    $page->del_db($id_t_del);
    //  var_dump($id_t_del);
    header('Location:index.php?id=admin');
}

if (isset($_POST['title_t']) && isset($_POST['descr_t']) && isset($_POST['text_t'])) {
    $title_t = filter_input(INPUT_POST, 'title_t', FILTER_SANITIZE_FULL_SPECIAL_CHARS);
    $descr_t = filter_input(INPUT_POST, 'descr_t', FILTER_SANITIZE_FULL_SPECIAL_CHARS);
    $text_t = filter_input(INPUT_POST, 'text_t', FILTER_SANITIZE_FULL_SPECIAL_CHARS);

    $page->up_db($title_t, $descr_t, $text_t);
    header('Location:index.php?id=admin');

}



if(isset($_GET['id']))
{
    if ($_GET['id'] == 'admin') {
        $text = $page->get_all($start_pos, $perpage);
        $pagination = $page->pagination($pages, $count_pages, $uri);
        echo $page->get_body($text, 'admin', $pagination);
    }
    if ($_GET['id'] == 'registration') {
        $text = null;
        $pagination = null;
        echo $page->get_body($text, 'registration', $pagination);
    }

    if ($_GET['id'] == 'authorization') {
        $text = null;
        $pagination = null;
        echo $page->get_body($text, 'authorization', $pagination);
    }


    $id = (int)$_GET['id'];


    if ($id !=0)
    {
        $text= $page->get_one($id);
        $pagination = null;

        echo $page->get_body($text, 'view', $pagination);
    }


} else {
    $text = $page->get_all($start_pos, $perpage);
    $pagination = $page->pagination($pages, $count_pages, $uri);
    echo $page->get_body($text, 'main', $pagination);
}







