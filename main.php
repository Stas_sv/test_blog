<?  include 'header.php' ?>
        <body>

<!-- Navigation -->
<nav class="navbar navbar-default navbar-custom navbar-fixed-top">
        <div class="container-fluid">
                <!-- Brand and toggle get grouped for better mobile display -->
                <div class="navbar-header page-scroll">
                        <button type="button" class="navbar-toggle" data-toggle="collapse" data-target="#bs-example-navbar-collapse-1">
                                <span class="sr-only">Toggle navigation</span>
                                Menu <i class="fa fa-bars"></i>
                        </button>

                </div>

                <!-- Collect the nav links, forms, and other content for toggling -->
                <div class="collapse navbar-collapse" id="bs-example-navbar-collapse-1">
                        <ul class="nav navbar-nav navbar-right">
                                <li>
                                        <a href="index.php">Домой</a>
                                </li>
                                <li>
                                        <a href="?id=admin">Админка</a>
                                </li>


                                <li>
                                        <a href="?id=authorization">Авторизация(Вход)</a>
                                </li>
                                <li>
                                        <a href="?id=registration">Регистрация</a>
                                </li>
                        </ul>
                </div>
                <!-- /.navbar-collapse -->
        </div>
        <!-- /.container -->
</nav>

<!-- Page Header -->
<!-- Set your background image for this header on the line below. -->
<header class="intro-header" style="background-image: url('img/home-bg.jpg')">
        <div class="container">
                <div class="row">
                        <div class="col-lg-8 col-lg-offset-2 col-md-10 col-md-offset-1">
                                <div class="site-heading">
                                        <h1>Тестовый Блог</h1>
                                        <hr class="small">
                                        <span class="subheading">Шаблон by Start Bootstrap</span>
                                </div>
                        </div>
                </div>
        </div>
</header>

<!-- Main Content -->






<div class="container">
        <div class="row">
                <div class="col-lg-8 col-lg-offset-2 col-md-10 col-md-offset-1">
                        <? if (isset($text)) : ?>
                                <? foreach ($text as $item) : ?>
                                        <div class="post-preview">
                                                <a href="index.php?id=<?=$item['id'];?>">
                                                        <h2 class="post-title">
                                                                <?=$item['title'];?>
                                                        </h2>
                                                        <h3 class="post-subtitle">
                                                                <?= $item['descr']; ?>

                                                        </h3>
                                                </a>
                                            <!-- <p class="post-meta">Posted by <a href="#">Start Bootstrap</a> on September 24, 2014</p> -->
                                        </div>
                                <hr>
                                <? endforeach; ?>
                        <? endif; ?>
                        <hr>
                        <!-- Pager -->

                        <ul class="pager">
                                <li class="next">
                                        <?= $pagination ?>
                                </li>
                        </ul>
                </div>
        </div>
</div>

<hr>

<?  include 'footer.php' ?>