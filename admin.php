<!--/**
 * Created by PhpStorm.
 * User: stas
 * Date: 18.10.16
 * Time: 16:21
 */-->
<? include 'header.php' ?>
<body>

<!-- Navigation -->
<nav class="navbar navbar-default navbar-custom navbar-fixed-top">
    <div class="container-fluid">
        <!-- Brand and toggle get grouped for better mobile display -->
        <div class="navbar-header page-scroll">
            <button type="button" class="navbar-toggle" data-toggle="collapse"
                    data-target="#bs-example-navbar-collapse-1">
                <span class="sr-only">Toggle navigation</span>
                Menu <i class="fa fa-bars"></i>
            </button>

        </div>

        <!-- Collect the nav links, forms, and other content for toggling -->
        <div class="collapse navbar-collapse" id="bs-example-navbar-collapse-1">
            <ul class="nav navbar-nav navbar-right">
                <li>
                    <a href="index.php">Главная</a>
                </li>
                <li>
                    <a href="?id=admin">Админка</a>
                </li>


                <li>
                    <a href="?id=authorization">Авторизация(Вход)</a>
                </li>
                <li>
                    <a href="?id=registration">Регистрация</a>
                </li>
            </ul>
        </div>
        <!-- /.navbar-collapse -->
    </div>
    <!-- /.container -->
</nav>

<!-- Page Header -->
<!-- Set your background image for this header on the line below. -->
<header class="intro-header" style="background-image: url('img/home-bg.jpg')">
    <div class="container">
        <div class="row">
            <div class="col-lg-8 col-lg-offset-2 col-md-10 col-md-offset-1">
                <div class="site-heading">
                    <h1>Тестовый Блог АДМИН ПАНЕЛЬ</h1>
                    <hr class="small">
                    <span class="subheading">Шаблон by Start Bootstrap</span>
                </div>
            </div>
        </div>
    </div>
</header>


<hr>
<form method="post" action="index.php" class="form-horizontal   ">
    <fieldset>

        <legend>Добавление записи в таблицу блога</legend>

        <!-- Form Name -->


        <!-- Text input
        <div class="form-group">
            <label class="col-md-4 control-label" for="ID">ID Number</label>
            <div class="col-md-4">
                <input id="ID" name="id_t" placeholder="№" class="form-control input-md" type="text">

            </div>
        </div>-->

        <!-- Text input-->
        <div class="form-group">
            <label class="col-md-1 control-label" for="TITLE">Название</label>
            <div class="col-md-8">
                <input id="TITLE" name="title_t" placeholder="Заголовок" class="form-control input-md" type="text">

            </div>
        </div>

        <!-- Text input-->
        <div class="form-group">
            <label class="col-md-1 control-label" for="DESCR">Краткое содержание</label>
            <div class="col-md-8">
                <input id="DESCR" name="descr_t" placeholder="Краткое содержание поста" class="form-control input-md"
                       type="text">

            </div>
        </div>

        <!-- Textarea -->
        <div class="form-group">
            <label class="col-md-1 control-label" for="Text">Пост(статья)</label>
            <div class="col-md-8">
                <textarea class="form-control" id="text_t" name="text_t" placeholder="Подробное описание"></textarea>
            </div>
        </div>

        <!-- Button -->
        <div class="form-group ">

            <div class="col-md-4">
                <input type="submit" value="Сохранить" id="singlebutton" name="singlebutton"
                       class="btn btn-success">
            </div>
        </div>

    </fieldset>
</form>
<hr>
<h3>Существующие статьи</h3>

<table class="tg">
    <tr style="background-color: #f38630; height: 50px;width: 150px">
        <th class="tg-yw4l" style="text-align: center">№(Удаление)</th>
        <th class="tg-yw4l" style="text-align: center">Название</th>
        <th class="tg-yw4l" style="text-align: center">Краткое содержание</th>
        <th class="tg-yw4l" style="text-align: center">Статья</th>
        <th class="tg-yw4l" style="text-align: center">Редактирование(Save)</th>
    </tr>
    <? if (isset($text)) : ?>
        <? foreach ($text as $item) : ?>

            <form method="post" action="index.php">
            <tr>
                <td class="tg-yw4l"><?= $item['id']; ?><input type="checkbox" name="id_t_del"
                                                              value="<?= $item['id']; ?>">
                    <input type="submit" value=" Удалить " class="btn btn-danger">
            </form>
            <form method="post" action="index.php">

                <div style="display: none"><input type="text" name="id_upd" value="<?= $item['id']; ?>"></div>
                </td>


                <td class="tg-yw4l"><textarea class="form-control" name="title_upd"><?= $item['title']; ?></textarea>
                </td>
                <td class="tg-yw4l"><textarea class="form-control" name="descr_upd"><?= $item['descr']; ?></textarea>
                </td>
                <td class="tg-yw4l"><textarea class="from-control" name="text_upd"> <?= $item['text']; ?></textarea>
                </td>
                <td class="tg-yw4l"><input type="submit" value="Записать изменения" class="btn btn-success"></td>
            </form>
            </tr>

        <? endforeach; ?>
    <? endif; ?>
</table>
<hr>
<!-- Pager -->

<ul class="pager">
    <li class="next">
        <?= $pagination ?>
    </li>
</ul>
<? include 'footer.php' ?>