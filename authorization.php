<!--
/**
 * Created by PhpStorm.
 * User: stas
 * Date: 23.10.16
 * Time: 20:00
 */-->
<? include 'header.php' ?>
<body>

<!-- Navigation -->
<nav class="navbar navbar-default navbar-custom navbar-fixed-top">
    <div class="container-fluid">
        <!-- Brand and toggle get grouped for better mobile display -->
        <div class="navbar-header page-scroll">
            <button type="button" class="navbar-toggle" data-toggle="collapse"
                    data-target="#bs-example-navbar-collapse-1">
                <span class="sr-only">Toggle navigation</span>
                Menu <i class="fa fa-bars"></i>
            </button>

        </div>

        <!-- Collect the nav links, forms, and other content for toggling -->
        <div class="collapse navbar-collapse" id="bs-example-navbar-collapse-1">
            <ul class="nav navbar-nav navbar-right">
                <li>
                    <a href="index.php">Домой</a>
                </li>
                <li>
                    <a href="?id=admin">Админка</a>
                </li>


                <li>
                    <a href="?id=authorization">Авторизация(Вход)</a>
                </li>
                <li>
                    <a href="?id=registration">Регистрация</a>
                </li>
            </ul>
        </div>
        <!-- /.navbar-collapse -->
    </div>
    <!-- /.container -->
</nav>

<!-- Page Header -->
<!-- Set your background image for this header on the line below. -->
<header class="intro-header" style="background-image: url('img/home-bg.jpg')">
    <div class="container">
        <div class="row">
            <div class="col-lg-8 col-lg-offset-2 col-md-10 col-md-offset-1">
                <div class="site-heading">
                    <h1>Тестовый Блог</h1>
                    <hr class="small">
                    <span class="subheading">Шаблон by Start Bootstrap</span>
                </div>
            </div>
        </div>
    </div>
</header>

<!-- Main Content -->
<form method="post" action="index.php" role="form">
    <div class="form-group">
        <label for="exampleInputLogin">Логин</label>
        <input type="text" class="form-control" id="exampleInputLogin" placeholder="Enter login" name="login">
    </div>
    <div class="form-group">
        <label for="exampleInputPassword1">Пароль</label>
        <input type="password" class="form-control" id="exampleInputPassword1" placeholder="Enter Password"
               name="password">
    </div>

    <div class="checkbox">
        <label>
            <input type="checkbox" name="not_attach_ip"> Не прикреплять к IP(не безопасно)
        </label>
    </div>
    <button type="submit" class="btn btn-default" name="submit" value="Войти">Отправить</button>
</form>
<? include 'footer.php' ?>
