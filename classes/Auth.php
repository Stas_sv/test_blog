<?php
/**
 * Created by PhpStorm.
 * User: stas
 * Date: 22.10.16
 * Time: 21:53
 */

namespace classes;


class  Auth
{
    private $db;


    public function __construct()
    {
        $this->db = new Database();
    }

    public function inspection_new_user($login, $password)
    {

        if (preg_match("/^[a-z0-9_-]{3,16}$/", $login)) {
            $quest = $this->db->inspection_login_to_existence($login);

            if ($quest = true) {
                $password = md5(md5(trim($password)));
                $newusr = $this->db->rec_new_user($login, $password);


            } else {
                exit("  какаято проблема");
            }
        } else {
            exit(" Невернонаписание логина [a-z0-9_-]{3,16} ");
        }
    }

    public function generateCode($length)
    {
        $chars = "abcdefghijklmnopqrstuvwxyzABCDEFGHI JKLMNOPRQSTUVWXYZ0123456789";
        $code = "";
        $clen = strlen($chars) - 1;
        while (strlen($code) < $length) {
            $code .= $chars[mt_rand(0, $clen)];
        }
        return $code;
    }

    public function authoriz($login, $password)
    {
        $data = $this->db->get_db_rec_entered_login($login);
        if ($data['user_password'] === md5(md5($password))) {
            $hash = md5($this->generateCode(10));


            $qwerty = $this->db->rec_in_db_new_hash($hash, $data);
            setcookie("id", $data['user_id'], time() + 60 * 60 * 24 * 30);
            setcookie("hash", $hash, time() + 60 * 60 * 24 * 30);
            header("Location: index.php");

            exit("Вы ввели ПППППППравильный логин/пароль");

        } else {
            setcookie("id", "", time() - 3600 * 24 * 30 * 12, "/");
            setcookie("hash", "", time() - 3600 * 24 * 30 * 12, "/");
            exit("Вы ввели неправильный логин/пароль");
        }

    }


    public function check_the_connection_of_loyalty()
    {
        $userdata = $this->db->check_loyalty();


        if (($userdata['user_hash'] !== $_COOKIE['hash']) or ($userdata['user_id'] !== filter_input(INPUT_COOKIE, 'id', FILTER_SANITIZE_NUMBER_INT))) {
            setcookie("id", "", time() - 3600 * 24 * 30 * 12, "/");
            setcookie("hash", "", time() - 3600 * 24 * 30 * 12, "/");
            print "Хм, что-то не получилось";
        } else {
            print "Привет, " . $userdata['user_login'] . ". Всё работает!";
        }
    }




}
