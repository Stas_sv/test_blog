<?php
/**
 * Created by PhpStorm.
 * User: stas
 * Date: 11.10.16
 * Time: 15:02
 */

namespace classes;


class Page
{
    public $text;
    private $db;

    public function __construct()
    {
        $this->db = new Database();
    }

    public function get_all($start_pos, $perpage)
        {
            $resultat = $this->db->get_all_db($start_pos, $perpage);


            return $resultat;


        }

    /*public function get_all()
    {
        $db = new Database(HOST,User,PASS, DB);
        $resultat = $db->get_all_db();
        return $resultat;
    }*/
    public function get_one($id)
    {

        $resultat = $this->db->get_one_db($id);
        return $resultat;
    }

    public function get_body($text, $file, $pagination)
    {
    ob_start();
        include $file.'.php';
    return ob_get_clean();

    }

    public function up_db($title_t, $descr_t, $text_t)
    {
        $up = $this->db->in_new_info_db($title_t, $descr_t, $text_t);
        return $up;
        //echo $up;
    }

    public function del_db($id_t_del)
    {
        $del = $this->db->del_from_db_by_id($id_t_del);
        return $del;
    }

    public function edit_db($title_upd, $descr_upd, $text_upd, $id_upd)
    {
        $edit_db = $this->db->editing_table_in_db($title_upd, $descr_upd, $text_upd, $id_upd);
        return $edit_db;
        // var_dump($text_upd);
    }

    public function count_goods_int()
    {
        $count_goods_int = $this->db->count_goods();
        //var_dump($count_goods_int);
        return $count_goods_int;
    }

    public function pagination($pages, $count_pages, $uri)
    {

        $forward = null;
        $page_2_right = null;
        $page_1_right = null;
        $page_1_left = null;
        $page_2_left = null;
        $back = null;

        if ($_SERVER['QUERY_STRING']) {
            foreach ($_GET as $key => $value) {
                // print_r( "$key=>$value");
                if ($key != 'pages') $uri .= "{$key}=$value&amp;";
                //var_dump($_SERVER['QUERY_STRING']);
            }
        }
        if ($pages > 1) {
            $back = "<li class='next'><a  href='{$uri}pages=" . ($pages - 1) . "'>Назад</a></li>";
        }
        if ($pages < $count_pages) {
            $forward = "<li class='next'><a href='{$uri}pages=" . ($pages + 1) . "'>Вперед</a></li>";
        }

        $start_page = "<li class='next'><a  href='{$uri}pages=1'>Первая</a></li>";


        $end_page = "<li class='next'><a  href='{$uri}pages={$count_pages}'>Последняя({$count_pages})</a></li>";

        /* if ($pages - 2 > 0) {
             $page_2_left = "<li class='next'><a  href='{$uri}pages=" . ($pages - 2) . "'>" . ($pages - 2) . "</a></li>";
         }*/
        /*if ($pages - 1 > 0) {
            $page_1_left = "<li class='next'><a  href='{$uri}pages=" . ($pages - 1) . "'>" . ($pages - 1) . "</a></li>";
        }*/
        /*if ($pages + 1 <= $count_pages) {
            $page_1_right = "<li class='next'><a  href='{$uri}pages=" . ($pages + 1) . "'>" . ($pages + 1) . "</a></li>";
        }*/
        /*if ($pages + 2 <= $count_pages) {
            $page_2_right = "<li class='next'> <a class='nav-link' href='{$uri}pages=" . ($pages + 2) . "'>" . ($pages + 2) . "</a></li>";
        }*/

        $pages = "<li class='next'><a href=''>($pages)</a></li>";


        return $end_page . $forward . /*$page_2_right .*/ /*$page_1_right .*/
        $pages /*. $page_1_left*/ . /*$page_2_left .*/
        $back . $start_page;
    }


}