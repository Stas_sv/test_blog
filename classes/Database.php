<?php
/**
 * Created by PhpStorm.
 * User: stas
 * Date: 11.10.16
 * Time: 15:00
 */

namespace classes;

class  Database
{

    private $db;

    public function __construct()
    {
        $this->db = mysqli_connect("localhost", "root", "", "blog_db");


        mysqli_query($this->db, "SET NAMES utf8");
        //return $this->db;
    }


    //Отображение страниц и контента
    public function get_all_db($start_pos, $perpage)
    {
        $sql = "SELECT id,title,descr,text FROM infostat LIMIT $start_pos,$perpage ";

        $result = mysqli_query($this->db, $sql);

        if (!$result) {
            return false;
        }
        for ($i = 0; $i < mysqli_num_rows($result); $i++) {
            $row[] = mysqli_fetch_array($result, MYSQLI_ASSOC);
        }


        mysqli_close($this->db);
        return $row;
    }

    public function get_one_db($id)
    {
        $sql = "SELECT id,title,text FROM infostat WHERE id='$id'";
        $result = mysqli_query($this->db, $sql);
        if (!$result) {
            return false;
        }
        $row = mysqli_fetch_array($result, MYSQLI_ASSOC);
        mysqli_close($this->db);
        return $row;
    }

// Работа с постами
    public function in_new_info_db($title_t, $descr_t, $text_t)
    {
        $sql = "INSERT INTO  infostat (title,descr,text) VALUES ('$title_t','$descr_t','$text_t')";
        $result = mysqli_query($this->db, $sql);
        mysqli_close($this->db);
        return $result;

    }

    public function del_from_db_by_id($id_t_del)
    {
        $sql = "DELETE  FROM infostat WHERE id='$id_t_del'";
        $result = mysqli_query($this->db, $sql);
        mysqli_close($this->db);
        return $result;
    }

    public function editing_table_in_db($title_upd, $descr_upd, $text_upd, $id_upd)
    {
        $sql = "UPDATE infostat SET title='$title_upd',descr='$descr_upd',text='$text_upd'WHERE id='$id_upd'";

        $result = mysqli_query($this->db, $sql);

        mysqli_close($this->db);
        return $result;

    }

//Авторизация
    public function inspection_login_to_existence($login)
    {
        $sql = "SELECT * FROM  users WHERE user_login='$login'";
        $result = mysqli_query($this->db, $sql);

        if (mysqli_num_rows($result) > 0) {

            echo "ЛОГИН СУЩЕСТВУЕТ";

            return false;

        }
        if (mysqli_num_rows($result) <= 0) {
            echo "НЕ СУЩЕСТВУЕТ И БУДЕТ СОЗДАН";
            return true;
        }

    }


    public function rec_new_user($login, $password)
    {
        $sql = "INSERT INTO users (user_login,user_password) VALUES ('$login','$password')";
        $result = mysqli_query($this->db, $sql);
        mysqli_close($this->db);

        return $result;
    }

    public function get_db_rec_entered_login($login)
    {
        $sql = "SELECT user_id, user_password FROM users WHERE user_login='$login' ";
        $result = mysqli_query($this->db, $sql);
        $data = mysqli_fetch_assoc($result);

        return $data;
    }

    public function rec_in_db_new_hash($hash, $data)
    {
        $sql = "UPDATE users SET user_hash='$hash' WHERE user_id='$data[user_id]'";
        $result = mysqli_query($this->db, $sql);

        return $result;

    }

    public function check_loyalty()
    {
        $sql = "SELECT  user_login,user_id,user_hash FROM users WHERE user_id = $_COOKIE[id]";
        $query = mysqli_query($this->db,$sql);
        $userdata = mysqli_fetch_assoc($query);


        return $userdata;

    }


    //страницы
    public function count_goods()
    {
        $sql = "SELECT COUNT(*) FROM infostat";
        $query = mysqli_query($this->db, $sql);
        $countgoods[] = mysqli_fetch_row($query);

        return $countgoods['0'];
        //var_dump($countgoods);

    }


}