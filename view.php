<!--/**
 * Created by PhpStorm.
 * User: stas
 * Date: 11.10.16
 * Time: 23:48
 */
-->
<?  include 'header.php' ?>

<body>

<!-- Navigation -->
<nav class="navbar navbar-default navbar-custom navbar-fixed-top">
    <div class="container-fluid">
        <!-- Brand and toggle get grouped for better mobile display -->
        <div class="navbar-header page-scroll">
            <button type="button" class="navbar-toggle" data-toggle="collapse" data-target="#bs-example-navbar-collapse-1">
                <span class="sr-only">Toggle navigation</span>
                Menu <i class="fa fa-bars"></i>
            </button>

        </div>

        <!-- Collect the nav links, forms, and other content for toggling -->
        <div class="collapse navbar-collapse" id="bs-example-navbar-collapse-1">
            <ul class="nav navbar-nav navbar-right">
                <li>
                    <a href="index.html">Домой</a>
                </li>


                <li>
                    <a href="contact.html">Контакты</a>
                </li>
            </ul>
        </div>
        <!-- /.navbar-collapse -->
    </div>
    <!-- /.container -->
</nav>

<!-- Page Header -->
<!-- Set your background image for this header on the line below. -->
<header class="intro-header" style="background-image: url('img/home-bg.jpg')">
    <div class="container">
        <div class="row">
            <div class="col-lg-8 col-lg-offset-2 col-md-10 col-md-offset-1">
                <div class="site-heading">
                    <h1>Тестовый Блог</h1>
                    <hr class="small">
                    <span class="subheading">Шаблон by Start Bootstrap</span>
                </div>
            </div>
        </div>
    </div>
</header>

<!-- Post Content -->
<article><? if (isset($text)) : ?>
    <div class="container">
        <div class="row">
            <div class="col-lg-8 col-lg-offset-2 col-md-10 col-md-offset-1">
                <h2><?=$text['title'];?></h2>

                <p><?=$text['text'];?></p>

            </div>
        </div>
    </div>
</article>
                    <? endif; ?>
<hr>

<!-- Footer -->
<?  include 'footer.php' ?>